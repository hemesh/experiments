var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://admin:password@mongo.testing/mongopoc',{'authSource': 'admin'}, function(err, db) {
    if (err) { console.error(err); }
    else {
          console.log("Connection successful");

          /*
          	Code to create collection and insert docs - INSERT
          */
          var collection = db.collection('test');
		  var doc1 = {'hello':'doc1'};
		  var doc2 = {'hello':'doc2'};
		  var lotsOfDocs = [{'hello':'doc3'}, {'hello':'doc4'}];

		  collection.insert(doc1);

		  collection.insert(doc2, {w:1}, function(err, result) {
		  	if(err){ return console.log('Error while insert', err); }

		  	console.log('Successful insert', result);
		  });

		  collection.insert(lotsOfDocs, {w:1}, function(err, result) {
		  	if(err){ return console.log('Error while insert', err); }

		  	console.log('Successful insert', result);
		  });
		  
		  /*
		   Code to update an existing doc - UPDATE
		  */
		var doc = {mykey:1, fieldtoupdate:1};

		collection.insert(doc, {w:1}, function(err, result) {
			collection.update({mykey:1}, {$set:{fieldtoupdate:2}}, {w:1}, function(err, result) {
				if(err){ return console.log('Error while update', err); }

		  		console.log('Successful update', result);
			});
		});

		var doc2 = {mykey:2, docs:[{doc1:1}]};

		collection.insert(doc2, {w:1}, function(err, result) {
			collection.update({mykey:2}, {$push:{docs:{doc2:1}}}, {w:1}, function(err, result) {
				if(err){ return console.log('Error while update', err); }

		  		console.log('Successful update', result);
			});
		});

		/*
		 Code to remove Docs - DELETE
		*/

		var docs = [{mykey:1}, {mykey:2}, {mykey:3}];

		collection.insert(docs, {w:1}, function(err, result) {
			if(err){ return console.log('Error while delete', err); }

		  	console.log('Successful delete', result);
		});

		collection.remove({mykey:1});

		collection.remove({mykey:2}, {w:1}, function(err, result) {
			if(err){ return console.log('Error while delete', err); }

		  	console.log('Successful delete', result);
		});

		collection.remove();

		/*
			Code to read docs - READ
		*/
		var docs = [{mykey:1}, {mykey:2}, {mykey:3}];

	  	collection.insert(docs, {w:1}, function(err, result) {

		    collection.find().toArray(function(err, items) {
		    	if(err){ return console.log('Error while read', err); }

		  		console.log('Successful read', items);
		    });

		    var stream = collection.find({mykey:{$ne:2}}).stream();
		    stream.on("data", function(item) {});
		    stream.on("end", function() {});

		    collection.findOne({mykey:1}, function(err, item) {
		    	if(err){ return console.log('Error while read', err); }

		  		console.log('Successful read ', item);
		    });

		});

       }
});