FROM node:boron
WORKDIR /usr/src/app
# Get all the code needed to run the app
COPY . /usr/src/app/
# ARG APP_ENV
# Copy dependency definitions
COPY package.json /usr/src/app/
RUN npm install && npm install pm2 -g
# EXPOSE 8087 5556
VOLUME /usr/src/app/data
# ENV APP_ENV dev
#RUN gulp seed
CMD ["pm2-docker", "start", "index.js"]